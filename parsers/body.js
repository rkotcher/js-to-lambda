const nodeParser = require('./node');

module.exports = function(body, freeVars=[]) {
  const result = body.reduce((memo, node) => {
    const parsedNode = nodeParser(node, freeVars);

    return memo + ' ' + parsedNode;
  }, '');

  return result.trim();
};
