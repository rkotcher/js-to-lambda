const name = require('./name');
const keywords = require('./keywords');

const nodetypes = [
  ...name,
  ...keywords
];

module.exports = function(node, freeVars) {
  const results = nodetypes.reduce(function(memo, fn) {
    const reduction = fn(node, freeVars);

    if (reduction) {
      memo = memo.concat(reduction);
    }

    return memo;
  }, []);

  // TODO: check to see if any attempts returned results.

  return results;
};
