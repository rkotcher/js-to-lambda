module.exports = function parseVar(node, freeVars) {
  const nodeParser = require('../node'); // eliminate circular deps

  if (node.start.value === 'var') {
    return nodeParser(node.definitions[0].value, freeVars);
  }
};
