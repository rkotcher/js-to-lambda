const varParser = require('./var');
const functionParser = require('./function');
const returnParser = require('./return');

const middleware = [
  varParser,
  functionParser,
  returnParser
];

const verified = middleware.map(function(unverified) {
  return function(kwNode, freeVars) {
    if (kwNode.start.type === 'keyword') {
      return unverified(kwNode, freeVars);
    }
  };
});

module.exports = verified;
