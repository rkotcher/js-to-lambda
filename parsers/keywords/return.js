module.exports = function parseReturn(node, freeVars) {
  const nodeParser = require('../node');

  if (node.start.value === 'return') {
    return nodeParser(node.value, freeVars);
  }
};
