const _ = require('underscore');

function formatFunction(args, expression) {
  return args.map(a => 'λ' + a).join('.') + '.' + expression;
}

module.exports = function parseFunction(node, freeVars) {
  const bodyParser = require('../body'); // eliminate circular deps

  if (node.start.value === 'function') {
    const argnames = node.argnames.map(a => a.name);
    const collisions = _.intersection(argnames, freeVars);

    if (collisions.length > 0) {
      throw new Error(`"${collisions[0]}" is already bound to a parent scope!`);
    }

    const parsedBody = bodyParser(node.body, freeVars.concat(argnames));

    return formatFunction(argnames, parsedBody);
  }
};
