const colors = require('colors');
const uglifyjs = require('uglify-js');

const bodyParser = require('./parsers/body');

/**
 * Usage:   node index.js <JS_FUNCTION>
 *
 * This module currently takes in one argument, a javascript program expressed
 * in a single line. The program is intended (and thus only tested) to work on
 * functions.
 *
 * Example 1:
 *
 * node index.js "var a = function(a, b) { return b; }"
 *     \
 *      --> λab.b
 *
 * Future TODO: This top-level seque
 */
console.log('👀 This is a work in progress');

try {
  const ast = uglifyjs.parse(process.argv[2]);

  const parsedBody = bodyParser(ast.body);

  console.log(parsedBody);
} catch (e) {
  console.error('❌ Input error: ' + e.message);
  console.error(colors.red(e));
}
